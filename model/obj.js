var mongoose = require("mongoose");

//connect to database
var db = mongoose.connect('mongodb://metauser:getameta@ds055555.mongolab.com:55555/metaobj');

//create schema for blog post
var objSchema = new mongoose.Schema({
  _file:  String,
  s3_key: String,
  s3_bucket:   String,
  comments: [{ body: String, date: Date }],
  date: { type: Date, default: Date.now },
  hidden: Boolean,
  meta: {
    accessed: Number,
  }
});

//compile schema to model
module.exports = db.model('obj', objSchema)
