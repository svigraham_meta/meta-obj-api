var express = require('express');
var logger = require('morgan');
var mongoose = require('mongoose');
var fs = require('fs');
var bodyParser = require('body-parser')
var basicAuth = require('basic-auth');
var stylus = require('stylus');
var nib = require('nib');
var cors = require('cors');
var exports = module.exports = {};

var AWS      = require('aws-sdk'),
    zlib     = require('zlib'),
    s3Stream = require('s3-upload-stream')(new AWS.S3());

// Set the client to be used for the upload.
AWS.config.loadFromPath('./config.json');


var ModelFile = require("./model/obj");
// var Schema = mongoose.Schema;
// mongoose.connect('mongodb://metauser:getameta@ds055555.mongolab.com:55555/metaobj');
//mongoose.connect('mongodb://metauser:getameta@ds021590-a0.mlab.com:21590,ds021590-a1.mlab.com:21590/meta_big_obj_test?replicaSet=rs-ds021590');
//mongoose.connect('mongodb://localhost');

var conn = mongoose.connection;
var Grid = require('gridfs-stream');
Grid.mongo = mongoose.mongo;

//grid-fs
var gfs = Grid(conn.db);

var app = express()
app.use(logger('dev'))
app.use(bodyParser.json({limit: '500mb'}));
app.use(bodyParser.urlencoded({limit: '500mb', extended: true}));



//basic auth
var auth = function (req, res, next) {
  function unauthorized(res) {
    res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
    return res.send(401);
  };

  var user = basicAuth(req);

  if (!user || !user.name || !user.pass) {
    return unauthorized(res);
  };

  if (user.name === 'metauser' && user.pass === 'getameta') {
    return next();
  } else {
    return unauthorized(res);
  };
};

//css
function compile(str, path) {
  return stylus(str)
    .set('filename', path)
    .use(nib())
}
app.set('views', __dirname + '/views')
app.set('view engine', 'jade')
app.use(stylus.middleware(
  { src: __dirname + '/public'
  , compile: compile
  }
));

//cors - for testing swagger editor from localhost
var whitelist = [/http:\/\/localhost\:\d+/,'null', 'https://meta-desktop-share.herokuapp.com'];


var corsOptions = {
  origin: function(origin, callback){
    function matchInArray(string, expressions) {
        var len = expressions.length,
            i = 0;
        for (; i < len; i++) {
            if (string !== undefined && string.match(expressions[i])) {
                return true;
            }
        }
        return false;
    };
    //console.log(origin);
    callback(null, (matchInArray(origin, whitelist)));
  }
};
app.use(cors(corsOptions));

//public folder
app.use(express.static(__dirname + '/public'));

/* At the top, with other redirect methods before other routes */
// app.get('*',function(req,res,next){
//   if(req.headers['x-forwarded-proto']!='https')
//     res.redirect('https://mypreferreddomain.com'+req.url)
//   else
//     next() /* Continue to other routes if we're not redirecting */
// })

app.get('/metaobj/file/metadata', auth, function(req,res){
  // get by filename
  ModelFile.find({ _file: req.query.filename }, function(err, obj) {
    if (err) throw err;

    // object of the user
    res.json(obj);
  });
});

app.get('/metaobj/file', auth, function(req,res){
  // download the file via aws s3 here
    var fileKey = req.query.filename;
    var targetFileName = req.query.target;

    console.log('Trying to download file', fileKey);

    var s3 = new AWS.S3();
    var options = {
        Bucket    : 'meta-nihon',
        Region: 'us-west-2',
        Key    : fileKey,
    };
    console.log(targetFileName);
    if (targetFileName != undefined && targetFileName != ''){
      var file = require('fs').createWriteStream(targetFileName);
      s3.getObject(options).createReadStream().pipe(file);
      res.send("File downloaded to "+targetFileName);
    }
    else {
      res.attachment(fileKey);
      var fileStream = s3.getObject(options).createReadStream();
      fileStream.pipe(res);
      res.send("Success");
    }
});

app.post('/metaobj/file', auth, function(req,res){

  //moving to S3 for file storage.
  //stores directly to S3 in a compressed form.

  headers = JSON.stringify(req.headers)
  //console.log(req.headers["x_filename"])
  var options = {filename : req.headers["x_filename"]}

  // Create the streams
  var read = fs.createReadStream(options.filename);
  var compress = zlib.createGzip();
  var upload = s3Stream.upload({
    "Bucket": "meta-nihon",
    "Key": options.filename+".gz"
  });

  // Optional configuration
  //upload.maxPartSize(20971520); // 20 MB
  upload.maxPartSize(41943040); //40 MB
  upload.concurrentParts(5);

  // Handle errors.
  upload.on('error', function (error) {
    console.log(error);
  });
  /* Handle progress. Example details object:
     { ETag: '"f9ef956c83756a80ad62f54ae5e7d34b"',
       PartNumber: 5,
       receivedSize: 29671068,
       uploadedSize: 29671068 }
  */
  upload.on('part', function (details) {
    console.log(details);
  });
  /* Handle upload completion. Example details object:
     { Location: 'https://bucketName.s3.amazonaws.com/filename.ext',
       Bucket: 'bucketName',
       Key: 'filename.ext',
       ETag: '"bf2acbedf84207d696c8da7dbb205b9f-5"' }
  */
  upload.on('uploaded', function (details) {
    console.log('uploaded...')
    console.log(details);
    //store meta data in mongo.
    var model_file = new ModelFile({_file: options.filename, s3_key: options.filename+".gz",
													s3_bucket: "meta-nihon"});

    //save model to MongoDB
    model_file.save(function (err) {
      if (err) {
    		return err;
      }
      else {
        res.send("success");
      	//console.log("Post saved");
      }
    //res.send("Success");
    });

  });
  // Pipe the incoming filestream through compression, and up to S3.
  read.pipe(compress).pipe(upload);

});


//deletes a file by filename
app.delete('/metaobj/file', auth, function(req,res){
  var options = {filename : req.query.filename}

});

app.get('/metaobj/files/metadata', auth, function(req,res){
  ModelFile.find({}, function(err, obj) {
    if (err) throw err;

    // object of the user
    res.json(obj);
  });
});

//catch all
app.get('*', function(req, res) {
  res.json({"message": "Not an API endpoint"});
});


app.use('/console',express.static(__dirname + '/public/dist/index.html'));
var server = app.listen(process.env.PORT || 5000)

exports.closeServer = function(){
  server.close();
};
// app.listen(3000, function(){
//   console.log('Express server listening on port 3000')
// })
