
var chai = require('chai');
chai.use(require('chai-json-schema'));

var request = require("request"),
    assert = chai.assert,
    expect = chai.expect,
    appServer = require("../express.js"),
    base_url = "http://localhost:5000/";

var metadataschema = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "id": "http://jsonschema.net",
  "type": "object",
  "properties": {
    "_id": {
      "id": "http://jsonschema.net/_id",
      "type": "string"
    },
    "_file": {
      "id": "http://jsonschema.net/_file",
      "type": "string"
    },
    "s3_key": {
      "id": "http://jsonschema.net/s3_key",
      "type": "string"
    },
    "s3_bucket": {
      "id": "http://jsonschema.net/s3_bucket",
      "type": "string"
    },
    "__v": {
      "id": "http://jsonschema.net/__v",
      "type": "integer"
    },
    "date": {
      "id": "http://jsonschema.net/date",
      "type": "string"
    },
    "comments": {
      "id": "http://jsonschema.net/comments",
      "type": "array",
      "items": []
    }
  },
  "required": [
    "_id",
    "_file",
    "s3_key",
    "s3_bucket",
    "__v",
    "date",
    "comments"
  ]
}


describe("Get metadata for all files", function() {

  describe("GET /metaobj/files/metadata", function() {

    it("has more than one model metadata", function(done) {
      request.get(base_url+"metaobj/files/metadata", function(error, response, body) {
        //console.log(body);
        console.log("checking for - has more than one model metadata...");
        //expect(body).toBe("Hello World");
        expect(JSON.parse(body)).to.have.length.above(1);
        //appServer.closeServer();
        done();
      })
      .auth('metauser', 'getameta');
    });

    it("responds with 200", function(done) {
      request.get(base_url+"metaobj/files/metadata", function(error, response, body) {
        //console.log(body);
        //expect(body).toBe("Hello World");
        console.log("checking for - returns 200...");
        expect(200);
        //appServer.closeServer();
        done();
      })
      .auth('metauser', 'getameta');
    });

    it("receives a valid schema for metadata from mongodb", function(done) {
      request.get(base_url+"metaobj/files/metadata", function(error, response, body) {
        //console.log(body);
        //expect(body).toBe("Hello World");
        var b = JSON.parse(body);
        var random = b[Math.floor(Math.random()*b.length)]
        console.log("checking for - valid schema...");
        expect(random).to.be.jsonSchema(metadataschema);
        //appServer.closeServer();
        done();
      })
      .auth('metauser', 'getameta');
    });


  });


});
