# Project

REST API for META to store and retrieve 3D object files

## API console
navigate to https://metaobjstore-api.herokuapp.com/dist/index.html

# Technology
Node (uses express), s3 for storage, mongodb for metadata(uses Mongolab for db service) and Swagger for console

# Running

```
npm install
node express.js
```

## Preferred way
1. Install the Heroku toolbelt. Will allow you to simulate production env.
2. ``` heroku local web ``` will start the node app on 5000
3. ``` npm test ``` will run the tests.

# Cloud Services
Currently -

  - Mongolab for metadata
  - Bitbucket for repo
  - S3 for storage
  - Heroku for deployment
  - Swagger for documentation (https://metaobjstore-api.herokuapp.com/dist/index.html)

Things we need to look into -

  - Key-value layer (for caching and gridID <-> Filename), can be hosted redis or memcache
  - Iron for background jobs (depending on the scope of the service)

# Updates/Changelog
12/1/2015

  - MVP completed for metaobj endpoint (GET /metaobj & POST /metaobj)
  - Creating a new object  (header x_filename is imporant for a multipart file upload workaround)
  ``` http -a metauser:getameta -f POST http://localhost:5000/metaobj/file file@airboat.obj x_filename:"airboat.obj" ```
  - Retrieving an object  
  ``` http -a metauser:getameta GET http://localhost:5000/metaobj/file filename='airboat.obj' ```

1/15/2015
  - End points change (/metaobj for bulk, /metaobj/file for individual)
2/27/2016
  - Swagger console integrated

4/23/2016
 - migrated to S3
 - mongoose model for meta data
 - no grid-fs needed anymore as storage is s3 and not mongo for large files
 - need to refactor the code.
 - Creating a new object
<code>
  http -a metauser:getameta -f POST http://localhost:5000/metaobj/file file@/Users/svigraham/scratch/EngineTestModel.obj x_filename:"/Users/svigraham/scratch/EngineTestModel.obj" --timeout=45

</code>

5/4/2016
 - tests with mocha and chai


# Things to do
(Not in any order of priority)

  - Define better resource model/convention/etc
  - Write Unity wrappers
  - More tests
